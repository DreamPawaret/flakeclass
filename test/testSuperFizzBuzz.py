import unittest
import superfizzbuzz

class TestSuperFizzBuzz(unittest.TestCase):
    
    def test_positive(self):
        for n in range(0, 10000, 1):
            result = ""
            if n % 9 == 0 and n % 25 == 0:
                result =  "FizzFizzBuzzBuzz"
            elif n % 3 == 0 and n % 5 == 0:
                result =  "FizzBuzz"
            elif n % 9 == 0:
                result =  "FizzFizz"
            elif n % 25 == 0:
                result =  "Buzz"    
            elif n%3 == 0:
                result =  "Fizz"
            elif n % 5 == 0:
                result =  "Buzz"
            else:
                result = "NoFizzBuzz"

            self.assertEqual(superfizzbuzz.superFizzBuzz(n), result)

    def test_negative(self):
        for n in range(0, -10000, -1):
            self.assertEqual(superfizzbuzz.superFizzBuss(n), "MustBePositiveInput")

if __name__ == '__main__':
    unittest.main()

